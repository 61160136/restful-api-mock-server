const express = require('express')
const router = express.Router()
const usersController = require('../controller/UsersController')

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.json(usersController.getUsers())
})

router.get('/:id', function (req, res, next) {
  const { id } = req.params
  res.json(usersController.getUser(id))
})

router.post('/', function (req, res, next) {
  const payload = req.body
  res.json(usersController.addUser(payload))
})

router.put('/', function (req, res, next) {
  const payload = req.body
  res.json(usersController.updateUser(payload))
})

router.delete('/:id', function (req, res, next) {
  const { id } = req.params
  res.json(usersController.deleteUser(id))
})




module.exports = router
